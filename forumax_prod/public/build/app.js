(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app"],{

/***/ "./assets/app.js":
/*!***********************!*\
  !*** ./assets/app.js ***!
  \***********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_array_reverse__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.array.reverse */ "./node_modules/core-js/modules/es.array.reverse.js");
/* harmony import */ var core_js_modules_es_array_reverse__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_reverse__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.date.to-string */ "./node_modules/core-js/modules/es.date.to-string.js");
/* harmony import */ var core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");
/* harmony import */ var core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_promise__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var regenerator_runtime_runtime__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! regenerator-runtime/runtime */ "./node_modules/regenerator-runtime/runtime.js");
/* harmony import */ var regenerator_runtime_runtime__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(regenerator_runtime_runtime__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _styles_app_scss__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./styles/app.scss */ "./assets/styles/app.scss");
/* harmony import */ var _styles_app_scss__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_styles_app_scss__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var bootstrap__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! bootstrap */ "./node_modules/bootstrap/dist/js/bootstrap.esm.js");








function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
// any CSS you import will output into a single css file (app.css in this case)
 // Problème de scope je crois
// import $ from 'jquery';



__webpack_require__(/*! bootstrap-icons/font/bootstrap-icons.css */ "./node_modules/bootstrap-icons/font/bootstrap-icons.css"); // Variables d'état


var currentPage = 'categories';
var currentCatId = '';
var currentTopicId = '';
var previousTitle = ''; // Onclick sur les categories pour afficher les topics correspondants

function setOnclickCatTitles() {
  $('.cat-title').on('click', /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
    var cat_id, cat_name;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            cat_id = this.getAttribute('cat-id');
            cat_name = this.getAttribute('cat-name');
            previousTitle = $('#current-title').text();
            $('#current-title').text(cat_name);
            $('#topic-page').show();
            $('#cat-page').hide();
            $('#return-btn').show();
            setTopics(cat_id);
            currentPage = 'topics';
            currentCatId = cat_id;

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  })));
} // Onclick sur les topics pour afficher les posts correspondants avec un fetch


function setOnclickTopicTitles() {
  $('.topic-title').on('click', /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
    var topic_id, topic_title;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            topic_id = this.getAttribute('topic-id');
            topic_title = this.getAttribute('topic-title');
            previousTitle = $('#current-title').text();
            $('#current-title').text(topic_title);
            $('#post-page').show();
            $('#topic-page').hide();
            setPosts(topic_id);
            currentPage = 'posts';
            currentTopicId = topic_id;

          case 9:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, this);
  })));
} // Onclick sur les croix pour supprimer un topic


function setOnclickDeleteTopics() {
  $('.delete-topic-btn').on('click', function () {
    fetch('/delete-topic/' + $(this).attr('id')).then(setTopics(currentCatId));
  });
} // Onclick sur les croix pour supprimer un post


function setOnclickDeletePosts() {
  $('.delete-post-btn').on('click', function () {
    fetch('/delete-post/' + $(this).attr('id')).then(setPosts(currentTopicId));
  });
} // Fetch et affiche les topics de la catégorie désirée


function setTopics(_x) {
  return _setTopics.apply(this, arguments);
} // Fetch et affiche les posts du topic désiré


function _setTopics() {
  _setTopics = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(cat_id) {
    var response, topic_list;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return fetch('/get-topics/' + cat_id);

          case 2:
            response = _context4.sent;
            _context4.next = 5;
            return response.json();

          case 5:
            topic_list = _context4.sent;
            $('#topic-list').text('');

            if (topic_list.topics.length == 0) {
              $('#topic-list').html('<p style="display: flex;justify-content: center;">Aucun topic dans ce forum : inscrivez ou connectez-vous et créez-en un !</p>');
            } // Affiche du plus récent au plus ancien


            topic_list.topics = topic_list.topics.reverse();
            topic_list.topics.forEach(function (topic) {
              // Topic infos
              var topic_infos = $('<div id="topic-infos">');
              topic_infos.append('<p>' + topic.auteur + '</p>'); // let date = new Date(topic.date);
              // topic_infos.append('<p>'+date.toLocaleString()+'</p>');

              var delete_topic = $('<i class="bi-x-square-fill delete-topic-btn ml-2" style="color: red;"></i>');
              delete_topic.attr('id', topic.id);

              if (role != 'ROLE_ADMIN') {
                delete_topic.hide();
              }

              topic_infos.append(delete_topic); // Topic container

              var topic_container = $('<div class="topic-container"></div>');
              topic_container.append('<a class="topic-title" topic-id="' + topic.id + '" topic-title="' + topic.titre + '">' + topic.titre + '</a>');
              topic_container.append(topic_infos);
              $('#topic-list').append(topic_container);
            });
            setOnclickTopicTitles();
            setOnclickDeleteTopics();

          case 12:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));
  return _setTopics.apply(this, arguments);
}

function setPosts(_x2) {
  return _setPosts.apply(this, arguments);
} // Bouton retour


function _setPosts() {
  _setPosts = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(topic_id) {
    var response, post_list;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return fetch('/get-posts/' + topic_id);

          case 2:
            response = _context5.sent;
            _context5.next = 5;
            return response.json();

          case 5:
            post_list = _context5.sent;
            $('#post-list').text('');
            post_list.posts.forEach(function (post) {
              // Post header
              var post_header = $('<div class="post-header"></div>');
              var author = $('<p>' + post.auteur + '</p>');
              var date = new Date(post.date);
              var date_string = $('<p>' + date.toLocaleString() + '</p>');
              var delete_post = $('<i class="bi-x-square-fill delete-post-btn" style="color: red;"></i>');
              delete_post.attr('id', post.id);

              if (role != 'ROLE_ADMIN') {
                delete_post.hide();
              }

              post_header.append(author, date_string, delete_post); // Post container

              var post_container = $('<div class="post-container"></div>');
              var text = $('<div class="text-container"><p id="' + post.id + '">' + post.texte + '</p></div>');
              post_container.append(post_header, text);
              $('#post-list').append(post_container);
            });
            setOnclickDeletePosts();

          case 9:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));
  return _setPosts.apply(this, arguments);
}

function returnBtn() {
  if (currentPage == 'topics') {
    $('#current-title').text("Bienvenue");
    $('#topic-page').hide();
    $('#cat-page').show();
    $('#return-btn').hide();
    currentPage = 'categories';
  } else if (currentPage == 'posts') {
    $('#current-title').text(previousTitle);
    $('#post-page').hide();
    $('#topic-page').show();
    setTopics(currentCatId);
    currentPage = 'topics';
  }
} // Affiche le pseudo après une connexion et initialise les fonctionnalités associées


function init_connexion(username, new_role) {
  // Pour tout les connectés
  $('#sign-in').modal('hide');
  $("#username").text(username);
  $("#send-topic-block").css("display", "flex");
  $("#send-post-block").css("display", "flex");
  $("#connect-block").hide();
  $("#user-block").show();
  $("#user-block").css('display', 'flex'); // Reservé au admins

  if (new_role == 'ROLE_ADMIN') {
    role = 'ROLE_ADMIN';
    $("#username").text($("#username").text() + ' [utilisateur]');
    $('.delete-topic-btn').show();
    $('.delete-post-btn').show();
    setOnclickDeleteTopics();
    setOnclickDeletePosts();
  } else if (new_role == 'ROLE_USER') {
    $("#username").text($("#username").text() + ' [administrateur]');
  }
} // Envoie un fetch pour se connecter avec le modal de connexion


function connexion() {
  var data = {
    username: $('#sign-in-username').val(),
    password: $('#sign-in-password').val()
  };
  fetch('/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then( /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(response) {
      var username, new_role;
      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              if (!response.ok) {
                _context3.next = 8;
                break;
              }

              username = data.username;
              _context3.next = 4;
              return response.text();

            case 4:
              new_role = _context3.sent;
              init_connexion(username, new_role);
              _context3.next = 9;
              break;

            case 8:
              $('#bad-credentials').css('display', 'inline');

            case 9:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3);
    }));

    return function (_x3) {
      return _ref3.apply(this, arguments);
    };
  }()) // A ameliorer
  ["catch"](function (error) {
    return console.log(error);
  });
} // Envoie un fetch pour se deconnecter (async nécéssaire pour éviter ns_binding_aborted )


function deconnexion() {
  return _deconnexion.apply(this, arguments);
} // Envoie un fetch pour s'inscrire avec le modal d'inscription


function _deconnexion() {
  _deconnexion = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
    var logout;
    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.next = 2;
            return fetch('/logout');

          case 2:
            logout = _context6.sent;
            location.reload();

          case 4:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  }));
  return _deconnexion.apply(this, arguments);
}

function inscription() {
  var data = {
    username: $('#sign-up-username').val(),
    password: $('#sign-up-password').val()
  };
  fetch('/register', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(function (response) {
    if (response.ok) {
      $("#sign-up").modal("hide");
    } else {
      $('#bad-username').css('display', 'inline');
    }
  }) // A ameliorer
  ["catch"](function (error) {
    return console.log(error);
  });
} // Envoie un fetch pour poster un topic


function send_topic() {
  var data = {
    title: $('#send-topic-title').val(),
    message: $('#send-topic-text').val(),
    cat_id: currentCatId
  };
  fetch('/send-topic', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(setTopics(currentCatId)) // A ameliorer
  ["catch"](function (error) {
    return console.log(error);
  });
} // Envoie un fetch pour envoyer un post


function send_post() {
  var data = {
    message: $('#send-post-text').val(),
    topic_id: currentTopicId
  };
  fetch('/send-a-post', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(setPosts(currentTopicId)) // A ameliorer
  ["catch"](function (error) {
    return console.log(error);
  });
}

$(function () {
  // On initialise les categories et on cache les autres volets
  setOnclickCatTitles();
  $('#topic-page').hide();
  $('#post-page').hide(); // Bouton retour

  $('#return-btn').hide();
  $('#return-btn').on('click', function () {
    returnBtn();
  }); // Connexion

  $('#sign-in-submit').on('click', function () {
    connexion();
  }); // Deconnexion

  $('#logout-btn').on('click', function () {
    deconnexion();
  }); // Inscription

  $('#sign-up-submit').on('click', function () {
    if ($('#sign-up-password').val() == $('#sign-up-password-2').val()) {
      inscription();
    } else {
      $('#bad-passwords').css('display', 'inline');
    }
  }); // Envoi topic

  $('#submit-topic').on('click', function () {
    send_topic();
  }); // Envoi post

  $('#submit-post').on('click', function () {
    send_post();
  });

  if (role == 'ROLE_USER') {
    $('#username').text($('#username').text() + ' [utilisateur]');
  } else if (role == 'ROLE_ADMIN') {
    $('#username').text($('#username').text() + ' [administrateur]');
  }
});

/***/ }),

/***/ "./assets/styles/app.scss":
/*!********************************!*\
  !*** ./assets/styles/app.scss ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

},[["./assets/app.js","runtime","vendors~app"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvYXBwLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9zdHlsZXMvYXBwLnNjc3MiXSwibmFtZXMiOlsicmVxdWlyZSIsImN1cnJlbnRQYWdlIiwiY3VycmVudENhdElkIiwiY3VycmVudFRvcGljSWQiLCJwcmV2aW91c1RpdGxlIiwic2V0T25jbGlja0NhdFRpdGxlcyIsIiQiLCJvbiIsImNhdF9pZCIsImdldEF0dHJpYnV0ZSIsImNhdF9uYW1lIiwidGV4dCIsInNob3ciLCJoaWRlIiwic2V0VG9waWNzIiwic2V0T25jbGlja1RvcGljVGl0bGVzIiwidG9waWNfaWQiLCJ0b3BpY190aXRsZSIsInNldFBvc3RzIiwic2V0T25jbGlja0RlbGV0ZVRvcGljcyIsImZldGNoIiwiYXR0ciIsInRoZW4iLCJzZXRPbmNsaWNrRGVsZXRlUG9zdHMiLCJyZXNwb25zZSIsImpzb24iLCJ0b3BpY19saXN0IiwidG9waWNzIiwibGVuZ3RoIiwiaHRtbCIsInJldmVyc2UiLCJmb3JFYWNoIiwidG9waWMiLCJ0b3BpY19pbmZvcyIsImFwcGVuZCIsImF1dGV1ciIsImRlbGV0ZV90b3BpYyIsImlkIiwicm9sZSIsInRvcGljX2NvbnRhaW5lciIsInRpdHJlIiwicG9zdF9saXN0IiwicG9zdHMiLCJwb3N0IiwicG9zdF9oZWFkZXIiLCJhdXRob3IiLCJkYXRlIiwiRGF0ZSIsImRhdGVfc3RyaW5nIiwidG9Mb2NhbGVTdHJpbmciLCJkZWxldGVfcG9zdCIsInBvc3RfY29udGFpbmVyIiwidGV4dGUiLCJyZXR1cm5CdG4iLCJpbml0X2Nvbm5leGlvbiIsInVzZXJuYW1lIiwibmV3X3JvbGUiLCJtb2RhbCIsImNzcyIsImNvbm5leGlvbiIsImRhdGEiLCJ2YWwiLCJwYXNzd29yZCIsIm1ldGhvZCIsImhlYWRlcnMiLCJib2R5IiwiSlNPTiIsInN0cmluZ2lmeSIsIm9rIiwiZXJyb3IiLCJjb25zb2xlIiwibG9nIiwiZGVjb25uZXhpb24iLCJsb2dvdXQiLCJsb2NhdGlvbiIsInJlbG9hZCIsImluc2NyaXB0aW9uIiwic2VuZF90b3BpYyIsInRpdGxlIiwibWVzc2FnZSIsInNlbmRfcG9zdCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0NBR0E7QUFDQTs7QUFDQTs7QUFDQUEsbUJBQU8sQ0FBQyx5R0FBRCxDQUFQLEMsQ0FHQTs7O0FBQ0EsSUFBSUMsV0FBVyxHQUFHLFlBQWxCO0FBQ0EsSUFBSUMsWUFBWSxHQUFHLEVBQW5CO0FBQ0EsSUFBSUMsY0FBYyxHQUFHLEVBQXJCO0FBQ0EsSUFBSUMsYUFBYSxHQUFHLEVBQXBCLEMsQ0FFQTs7QUFDQSxTQUFTQyxtQkFBVCxHQUErQjtBQUMzQkMsR0FBQyxDQUFDLFlBQUQsQ0FBRCxDQUFnQkMsRUFBaEIsQ0FBbUIsT0FBbkIsdUVBQTJCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNuQkMsa0JBRG1CLEdBQ1YsS0FBS0MsWUFBTCxDQUFrQixRQUFsQixDQURVO0FBRW5CQyxvQkFGbUIsR0FFUixLQUFLRCxZQUFMLENBQWtCLFVBQWxCLENBRlE7QUFHdkJMLHlCQUFhLEdBQUdFLENBQUMsQ0FBQyxnQkFBRCxDQUFELENBQW9CSyxJQUFwQixFQUFoQjtBQUNBTCxhQUFDLENBQUMsZ0JBQUQsQ0FBRCxDQUFvQkssSUFBcEIsQ0FBeUJELFFBQXpCO0FBQ0FKLGFBQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUJNLElBQWpCO0FBQ0FOLGFBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZU8sSUFBZjtBQUNBUCxhQUFDLENBQUMsYUFBRCxDQUFELENBQWlCTSxJQUFqQjtBQUNBRSxxQkFBUyxDQUFDTixNQUFELENBQVQ7QUFDQVAsdUJBQVcsR0FBRyxRQUFkO0FBQ0FDLHdCQUFZLEdBQUdNLE1BQWY7O0FBVnVCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQTNCO0FBWUgsQyxDQUVEOzs7QUFDQSxTQUFTTyxxQkFBVCxHQUFpQztBQUM3QlQsR0FBQyxDQUFDLGNBQUQsQ0FBRCxDQUFrQkMsRUFBbEIsQ0FBcUIsT0FBckIsdUVBQTZCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNyQlMsb0JBRHFCLEdBQ1YsS0FBS1AsWUFBTCxDQUFrQixVQUFsQixDQURVO0FBRXJCUSx1QkFGcUIsR0FFUCxLQUFLUixZQUFMLENBQWtCLGFBQWxCLENBRk87QUFHekJMLHlCQUFhLEdBQUdFLENBQUMsQ0FBQyxnQkFBRCxDQUFELENBQW9CSyxJQUFwQixFQUFoQjtBQUNBTCxhQUFDLENBQUMsZ0JBQUQsQ0FBRCxDQUFvQkssSUFBcEIsQ0FBeUJNLFdBQXpCO0FBQ0FYLGFBQUMsQ0FBQyxZQUFELENBQUQsQ0FBZ0JNLElBQWhCO0FBQ0FOLGFBQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUJPLElBQWpCO0FBQ0FLLG9CQUFRLENBQUNGLFFBQUQsQ0FBUjtBQUNBZix1QkFBVyxHQUFHLE9BQWQ7QUFDQUUsMEJBQWMsR0FBR2EsUUFBakI7O0FBVHlCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQTdCO0FBV0gsQyxDQUVEOzs7QUFDQSxTQUFTRyxzQkFBVCxHQUFrQztBQUM5QmIsR0FBQyxDQUFDLG1CQUFELENBQUQsQ0FBdUJDLEVBQXZCLENBQTBCLE9BQTFCLEVBQW1DLFlBQVc7QUFDMUNhLFNBQUssQ0FBQyxtQkFBaUJkLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWUsSUFBUixDQUFhLElBQWIsQ0FBbEIsQ0FBTCxDQUNDQyxJQURELENBQ01SLFNBQVMsQ0FBQ1osWUFBRCxDQURmO0FBRUgsR0FIRDtBQUlILEMsQ0FFRDs7O0FBQ0EsU0FBU3FCLHFCQUFULEdBQWlDO0FBQzdCakIsR0FBQyxDQUFDLGtCQUFELENBQUQsQ0FBc0JDLEVBQXRCLENBQXlCLE9BQXpCLEVBQWtDLFlBQVc7QUFDekNhLFNBQUssQ0FBQyxrQkFBZ0JkLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWUsSUFBUixDQUFhLElBQWIsQ0FBakIsQ0FBTCxDQUNDQyxJQURELENBQ01KLFFBQVEsQ0FBQ2YsY0FBRCxDQURkO0FBRUgsR0FIRDtBQUlILEMsQ0FFRDs7O1NBQ2VXLFM7O0VBK0JmOzs7O3VFQS9CQSxrQkFBeUJOLE1BQXpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBQ3lCWSxLQUFLLENBQUMsaUJBQWVaLE1BQWhCLENBRDlCOztBQUFBO0FBQ1FnQixvQkFEUjtBQUFBO0FBQUEsbUJBRTJCQSxRQUFRLENBQUNDLElBQVQsRUFGM0I7O0FBQUE7QUFFUUMsc0JBRlI7QUFHSXBCLGFBQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUJLLElBQWpCLENBQXNCLEVBQXRCOztBQUNBLGdCQUFHZSxVQUFVLENBQUNDLE1BQVgsQ0FBa0JDLE1BQWxCLElBQTRCLENBQS9CLEVBQWlDO0FBQzdCdEIsZUFBQyxDQUFDLGFBQUQsQ0FBRCxDQUFpQnVCLElBQWpCLENBQXNCLGdJQUF0QjtBQUNILGFBTkwsQ0FPSTs7O0FBQ0FILHNCQUFVLENBQUNDLE1BQVgsR0FBb0JELFVBQVUsQ0FBQ0MsTUFBWCxDQUFrQkcsT0FBbEIsRUFBcEI7QUFDQUosc0JBQVUsQ0FBQ0MsTUFBWCxDQUFrQkksT0FBbEIsQ0FBMEIsVUFBQUMsS0FBSyxFQUFJO0FBQy9CO0FBQ0Esa0JBQUlDLFdBQVcsR0FBRzNCLENBQUMsQ0FBQyx3QkFBRCxDQUFuQjtBQUNBMkIseUJBQVcsQ0FBQ0MsTUFBWixDQUFtQixRQUFNRixLQUFLLENBQUNHLE1BQVosR0FBbUIsTUFBdEMsRUFIK0IsQ0FJL0I7QUFDQTs7QUFDQSxrQkFBSUMsWUFBWSxHQUFHOUIsQ0FBQyxDQUFDLDRFQUFELENBQXBCO0FBQ0E4QiwwQkFBWSxDQUFDZixJQUFiLENBQWtCLElBQWxCLEVBQXVCVyxLQUFLLENBQUNLLEVBQTdCOztBQUNBLGtCQUFHQyxJQUFJLElBQUksWUFBWCxFQUF3QjtBQUNwQkYsNEJBQVksQ0FBQ3ZCLElBQWI7QUFDSDs7QUFDRG9CLHlCQUFXLENBQUNDLE1BQVosQ0FBbUJFLFlBQW5CLEVBWCtCLENBWS9COztBQUNBLGtCQUFJRyxlQUFlLEdBQUdqQyxDQUFDLENBQUMscUNBQUQsQ0FBdkI7QUFDQWlDLDZCQUFlLENBQUNMLE1BQWhCLENBQXVCLHNDQUFvQ0YsS0FBSyxDQUFDSyxFQUExQyxHQUE2QyxpQkFBN0MsR0FBK0RMLEtBQUssQ0FBQ1EsS0FBckUsR0FBMkUsSUFBM0UsR0FBZ0ZSLEtBQUssQ0FBQ1EsS0FBdEYsR0FBNEYsTUFBbkg7QUFDQUQsNkJBQWUsQ0FBQ0wsTUFBaEIsQ0FBdUJELFdBQXZCO0FBQ0EzQixlQUFDLENBQUMsYUFBRCxDQUFELENBQWlCNEIsTUFBakIsQ0FBd0JLLGVBQXhCO0FBQ0gsYUFqQkQ7QUFrQkF4QixpQ0FBcUI7QUFDckJJLGtDQUFzQjs7QUE1QjFCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEc7Ozs7U0FnQ2VELFE7O0VBMEJmOzs7O3NFQTFCQSxrQkFBd0JGLFFBQXhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBQ3lCSSxLQUFLLENBQUMsZ0JBQWNKLFFBQWYsQ0FEOUI7O0FBQUE7QUFDUVEsb0JBRFI7QUFBQTtBQUFBLG1CQUUwQkEsUUFBUSxDQUFDQyxJQUFULEVBRjFCOztBQUFBO0FBRVFnQixxQkFGUjtBQUdJbkMsYUFBQyxDQUFDLFlBQUQsQ0FBRCxDQUFnQkssSUFBaEIsQ0FBcUIsRUFBckI7QUFDQThCLHFCQUFTLENBQUNDLEtBQVYsQ0FBZ0JYLE9BQWhCLENBQXdCLFVBQUFZLElBQUksRUFBSTtBQUM1QjtBQUNBLGtCQUFJQyxXQUFXLEdBQUd0QyxDQUFDLENBQUMsaUNBQUQsQ0FBbkI7QUFDQSxrQkFBSXVDLE1BQU0sR0FBR3ZDLENBQUMsQ0FBQyxRQUFNcUMsSUFBSSxDQUFDUixNQUFYLEdBQWtCLE1BQW5CLENBQWQ7QUFDQSxrQkFBSVcsSUFBSSxHQUFHLElBQUlDLElBQUosQ0FBU0osSUFBSSxDQUFDRyxJQUFkLENBQVg7QUFDQSxrQkFBSUUsV0FBVyxHQUFHMUMsQ0FBQyxDQUFDLFFBQU13QyxJQUFJLENBQUNHLGNBQUwsRUFBTixHQUE0QixNQUE3QixDQUFuQjtBQUNBLGtCQUFJQyxXQUFXLEdBQUc1QyxDQUFDLENBQUMsc0VBQUQsQ0FBbkI7QUFDQTRDLHlCQUFXLENBQUM3QixJQUFaLENBQWlCLElBQWpCLEVBQXNCc0IsSUFBSSxDQUFDTixFQUEzQjs7QUFDQSxrQkFBR0MsSUFBSSxJQUFJLFlBQVgsRUFBd0I7QUFDcEJZLDJCQUFXLENBQUNyQyxJQUFaO0FBQ0g7O0FBQ0QrQix5QkFBVyxDQUFDVixNQUFaLENBQW1CVyxNQUFuQixFQUEyQkcsV0FBM0IsRUFBd0NFLFdBQXhDLEVBWDRCLENBWTVCOztBQUNBLGtCQUFJQyxjQUFjLEdBQUc3QyxDQUFDLENBQUMsb0NBQUQsQ0FBdEI7QUFDQSxrQkFBSUssSUFBSSxHQUFHTCxDQUFDLENBQUMsd0NBQXNDcUMsSUFBSSxDQUFDTixFQUEzQyxHQUE4QyxJQUE5QyxHQUFtRE0sSUFBSSxDQUFDUyxLQUF4RCxHQUE4RCxZQUEvRCxDQUFaO0FBQ0FELDRCQUFjLENBQUNqQixNQUFmLENBQXNCVSxXQUF0QixFQUFtQ2pDLElBQW5DO0FBRUFMLGVBQUMsQ0FBQyxZQUFELENBQUQsQ0FBZ0I0QixNQUFoQixDQUF1QmlCLGNBQXZCO0FBQ0gsYUFsQkQ7QUFtQkE1QixpQ0FBcUI7O0FBdkJ6QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHOzs7O0FBMkJBLFNBQVM4QixTQUFULEdBQW9CO0FBQ2hCLE1BQUdwRCxXQUFXLElBQUksUUFBbEIsRUFBMkI7QUFDdkJLLEtBQUMsQ0FBQyxnQkFBRCxDQUFELENBQW9CSyxJQUFwQixDQUF5QixXQUF6QjtBQUNBTCxLQUFDLENBQUMsYUFBRCxDQUFELENBQWlCTyxJQUFqQjtBQUNBUCxLQUFDLENBQUMsV0FBRCxDQUFELENBQWVNLElBQWY7QUFDQU4sS0FBQyxDQUFDLGFBQUQsQ0FBRCxDQUFpQk8sSUFBakI7QUFDQVosZUFBVyxHQUFHLFlBQWQ7QUFDSCxHQU5ELE1BTU0sSUFBR0EsV0FBVyxJQUFJLE9BQWxCLEVBQTBCO0FBQzVCSyxLQUFDLENBQUMsZ0JBQUQsQ0FBRCxDQUFvQkssSUFBcEIsQ0FBeUJQLGFBQXpCO0FBQ0FFLEtBQUMsQ0FBQyxZQUFELENBQUQsQ0FBZ0JPLElBQWhCO0FBQ0FQLEtBQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUJNLElBQWpCO0FBQ0FFLGFBQVMsQ0FBQ1osWUFBRCxDQUFUO0FBQ0FELGVBQVcsR0FBRyxRQUFkO0FBQ0g7QUFDSixDLENBRUQ7OztBQUNBLFNBQVNxRCxjQUFULENBQXdCQyxRQUF4QixFQUFrQ0MsUUFBbEMsRUFBNEM7QUFDeEM7QUFDQWxELEdBQUMsQ0FBQyxVQUFELENBQUQsQ0FBY21ELEtBQWQsQ0FBb0IsTUFBcEI7QUFDQW5ELEdBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZUssSUFBZixDQUFvQjRDLFFBQXBCO0FBQ0FqRCxHQUFDLENBQUMsbUJBQUQsQ0FBRCxDQUF1Qm9ELEdBQXZCLENBQTJCLFNBQTNCLEVBQXFDLE1BQXJDO0FBQ0FwRCxHQUFDLENBQUMsa0JBQUQsQ0FBRCxDQUFzQm9ELEdBQXRCLENBQTBCLFNBQTFCLEVBQW9DLE1BQXBDO0FBQ0FwRCxHQUFDLENBQUMsZ0JBQUQsQ0FBRCxDQUFvQk8sSUFBcEI7QUFDQVAsR0FBQyxDQUFDLGFBQUQsQ0FBRCxDQUFpQk0sSUFBakI7QUFDQU4sR0FBQyxDQUFDLGFBQUQsQ0FBRCxDQUFpQm9ELEdBQWpCLENBQXFCLFNBQXJCLEVBQStCLE1BQS9CLEVBUndDLENBU3hDOztBQUNBLE1BQUdGLFFBQVEsSUFBSSxZQUFmLEVBQTRCO0FBQ3hCbEIsUUFBSSxHQUFHLFlBQVA7QUFDQWhDLEtBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZUssSUFBZixDQUFvQkwsQ0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFlSyxJQUFmLEtBQXNCLGdCQUExQztBQUNBTCxLQUFDLENBQUMsbUJBQUQsQ0FBRCxDQUF1Qk0sSUFBdkI7QUFDQU4sS0FBQyxDQUFDLGtCQUFELENBQUQsQ0FBc0JNLElBQXRCO0FBQ0FPLDBCQUFzQjtBQUN0QkkseUJBQXFCO0FBQ3hCLEdBUEQsTUFPTSxJQUFHaUMsUUFBUSxJQUFJLFdBQWYsRUFBMkI7QUFDN0JsRCxLQUFDLENBQUMsV0FBRCxDQUFELENBQWVLLElBQWYsQ0FBb0JMLENBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZUssSUFBZixLQUFzQixtQkFBMUM7QUFDSDtBQUNKLEMsQ0FFRDs7O0FBQ0EsU0FBU2dELFNBQVQsR0FBcUI7QUFDakIsTUFBTUMsSUFBSSxHQUFHO0FBQ1RMLFlBQVEsRUFBRWpELENBQUMsQ0FBQyxtQkFBRCxDQUFELENBQXVCdUQsR0FBdkIsRUFERDtBQUVUQyxZQUFRLEVBQUV4RCxDQUFDLENBQUMsbUJBQUQsQ0FBRCxDQUF1QnVELEdBQXZCO0FBRkQsR0FBYjtBQUlBekMsT0FBSyxDQUFDLFFBQUQsRUFBVztBQUNaMkMsVUFBTSxFQUFFLE1BREk7QUFFWkMsV0FBTyxFQUFFO0FBQ0wsc0JBQWdCO0FBRFgsS0FGRztBQUtaQyxRQUFJLEVBQUVDLElBQUksQ0FBQ0MsU0FBTCxDQUFlUCxJQUFmO0FBTE0sR0FBWCxDQUFMLENBT0N0QyxJQVBEO0FBQUEsd0VBT08sa0JBQWVFLFFBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBQ0NBLFFBQVEsQ0FBQzRDLEVBRFY7QUFBQTtBQUFBO0FBQUE7O0FBRUtiLHNCQUZMLEdBRWdCSyxJQUFJLENBQUNMLFFBRnJCO0FBQUE7QUFBQSxxQkFHc0IvQixRQUFRLENBQUNiLElBQVQsRUFIdEI7O0FBQUE7QUFHSzZDLHNCQUhMO0FBSUNGLDRCQUFjLENBQUNDLFFBQUQsRUFBV0MsUUFBWCxDQUFkO0FBSkQ7QUFBQTs7QUFBQTtBQU1DbEQsZUFBQyxDQUFDLGtCQUFELENBQUQsQ0FBc0JvRCxHQUF0QixDQUEwQixTQUExQixFQUFvQyxRQUFwQzs7QUFORDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQVBQOztBQUFBO0FBQUE7QUFBQTtBQUFBLE9BZ0JBO0FBaEJBLFlBaUJPLFVBQUFXLEtBQUs7QUFBQSxXQUFJQyxPQUFPLENBQUNDLEdBQVIsQ0FBWUYsS0FBWixDQUFKO0FBQUEsR0FqQlo7QUFrQkgsQyxDQUVEOzs7U0FDZUcsVzs7RUFLZjs7Ozt5RUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUN1QnBELEtBQUssQ0FBQyxTQUFELENBRDVCOztBQUFBO0FBQ1FxRCxrQkFEUjtBQUVJQyxvQkFBUSxDQUFDQyxNQUFUOztBQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEc7Ozs7QUFNQSxTQUFTQyxXQUFULEdBQXVCO0FBQ25CLE1BQU1oQixJQUFJLEdBQUc7QUFDVEwsWUFBUSxFQUFFakQsQ0FBQyxDQUFDLG1CQUFELENBQUQsQ0FBdUJ1RCxHQUF2QixFQUREO0FBRVRDLFlBQVEsRUFBRXhELENBQUMsQ0FBQyxtQkFBRCxDQUFELENBQXVCdUQsR0FBdkI7QUFGRCxHQUFiO0FBSUF6QyxPQUFLLENBQUMsV0FBRCxFQUFjO0FBQ2YyQyxVQUFNLEVBQUUsTUFETztBQUVmQyxXQUFPLEVBQUU7QUFDTCxzQkFBZ0I7QUFEWCxLQUZNO0FBS2ZDLFFBQUksRUFBRUMsSUFBSSxDQUFDQyxTQUFMLENBQWVQLElBQWY7QUFMUyxHQUFkLENBQUwsQ0FPQ3RDLElBUEQsQ0FPTyxVQUFTRSxRQUFULEVBQW1CO0FBQ3RCLFFBQUlBLFFBQVEsQ0FBQzRDLEVBQWIsRUFBaUI7QUFDYjlELE9BQUMsQ0FBQyxVQUFELENBQUQsQ0FBY21ELEtBQWQsQ0FBb0IsTUFBcEI7QUFDSCxLQUZELE1BRU87QUFDSG5ELE9BQUMsQ0FBQyxlQUFELENBQUQsQ0FBbUJvRCxHQUFuQixDQUF1QixTQUF2QixFQUFpQyxRQUFqQztBQUNIO0FBQ0osR0FiRCxFQWNBO0FBZEEsWUFlTyxVQUFBVyxLQUFLO0FBQUEsV0FBSUMsT0FBTyxDQUFDQyxHQUFSLENBQVlGLEtBQVosQ0FBSjtBQUFBLEdBZlo7QUFnQkgsQyxDQUVEOzs7QUFDQSxTQUFTUSxVQUFULEdBQXNCO0FBQ2xCLE1BQU1qQixJQUFJLEdBQUc7QUFDVGtCLFNBQUssRUFBRXhFLENBQUMsQ0FBQyxtQkFBRCxDQUFELENBQXVCdUQsR0FBdkIsRUFERTtBQUVUa0IsV0FBTyxFQUFFekUsQ0FBQyxDQUFDLGtCQUFELENBQUQsQ0FBc0J1RCxHQUF0QixFQUZBO0FBR1RyRCxVQUFNLEVBQUVOO0FBSEMsR0FBYjtBQUtBa0IsT0FBSyxDQUFDLGFBQUQsRUFBZ0I7QUFDakIyQyxVQUFNLEVBQUUsTUFEUztBQUVqQkMsV0FBTyxFQUFFO0FBQ0wsc0JBQWdCO0FBRFgsS0FGUTtBQUtqQkMsUUFBSSxFQUFFQyxJQUFJLENBQUNDLFNBQUwsQ0FBZVAsSUFBZjtBQUxXLEdBQWhCLENBQUwsQ0FPQ3RDLElBUEQsQ0FPTVIsU0FBUyxDQUFDWixZQUFELENBUGYsRUFRQTtBQVJBLFlBU08sVUFBQW1FLEtBQUs7QUFBQSxXQUFJQyxPQUFPLENBQUNDLEdBQVIsQ0FBWUYsS0FBWixDQUFKO0FBQUEsR0FUWjtBQVVILEMsQ0FFRDs7O0FBQ0EsU0FBU1csU0FBVCxHQUFxQjtBQUNqQixNQUFNcEIsSUFBSSxHQUFHO0FBQ1RtQixXQUFPLEVBQUV6RSxDQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQnVELEdBQXJCLEVBREE7QUFFVDdDLFlBQVEsRUFBRWI7QUFGRCxHQUFiO0FBSUFpQixPQUFLLENBQUMsY0FBRCxFQUFpQjtBQUNsQjJDLFVBQU0sRUFBRSxNQURVO0FBRWxCQyxXQUFPLEVBQUU7QUFDTCxzQkFBZ0I7QUFEWCxLQUZTO0FBS2xCQyxRQUFJLEVBQUVDLElBQUksQ0FBQ0MsU0FBTCxDQUFlUCxJQUFmO0FBTFksR0FBakIsQ0FBTCxDQU9DdEMsSUFQRCxDQU9NSixRQUFRLENBQUNmLGNBQUQsQ0FQZCxFQVFBO0FBUkEsWUFTTyxVQUFBa0UsS0FBSztBQUFBLFdBQUlDLE9BQU8sQ0FBQ0MsR0FBUixDQUFZRixLQUFaLENBQUo7QUFBQSxHQVRaO0FBVUg7O0FBRUQvRCxDQUFDLENBQUMsWUFBVTtBQUVSO0FBQ0FELHFCQUFtQjtBQUNuQkMsR0FBQyxDQUFDLGFBQUQsQ0FBRCxDQUFpQk8sSUFBakI7QUFDQVAsR0FBQyxDQUFDLFlBQUQsQ0FBRCxDQUFnQk8sSUFBaEIsR0FMUSxDQU9SOztBQUNBUCxHQUFDLENBQUMsYUFBRCxDQUFELENBQWlCTyxJQUFqQjtBQUNBUCxHQUFDLENBQUMsYUFBRCxDQUFELENBQWlCQyxFQUFqQixDQUFvQixPQUFwQixFQUE2QixZQUFVO0FBQ25DOEMsYUFBUztBQUNaLEdBRkQsRUFUUSxDQWFSOztBQUNBL0MsR0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUJDLEVBQXJCLENBQXdCLE9BQXhCLEVBQWlDLFlBQVU7QUFDdkNvRCxhQUFTO0FBQ1osR0FGRCxFQWRRLENBa0JSOztBQUNBckQsR0FBQyxDQUFDLGFBQUQsQ0FBRCxDQUFpQkMsRUFBakIsQ0FBb0IsT0FBcEIsRUFBNkIsWUFBVTtBQUNuQ2lFLGVBQVc7QUFDZCxHQUZELEVBbkJRLENBdUJSOztBQUNBbEUsR0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUJDLEVBQXJCLENBQXdCLE9BQXhCLEVBQWlDLFlBQVU7QUFDdkMsUUFBSUQsQ0FBQyxDQUFDLG1CQUFELENBQUQsQ0FBdUJ1RCxHQUF2QixNQUFnQ3ZELENBQUMsQ0FBQyxxQkFBRCxDQUFELENBQXlCdUQsR0FBekIsRUFBcEMsRUFBb0U7QUFDaEVlLGlCQUFXO0FBQ2QsS0FGRCxNQUVLO0FBQ0R0RSxPQUFDLENBQUMsZ0JBQUQsQ0FBRCxDQUFvQm9ELEdBQXBCLENBQXdCLFNBQXhCLEVBQWtDLFFBQWxDO0FBQ0g7QUFDSixHQU5ELEVBeEJRLENBZ0NSOztBQUNBcEQsR0FBQyxDQUFDLGVBQUQsQ0FBRCxDQUFtQkMsRUFBbkIsQ0FBc0IsT0FBdEIsRUFBK0IsWUFBVTtBQUNyQ3NFLGNBQVU7QUFDYixHQUZELEVBakNRLENBcUNSOztBQUNBdkUsR0FBQyxDQUFDLGNBQUQsQ0FBRCxDQUFrQkMsRUFBbEIsQ0FBcUIsT0FBckIsRUFBOEIsWUFBVTtBQUNwQ3lFLGFBQVM7QUFDWixHQUZEOztBQUlBLE1BQUcxQyxJQUFJLElBQUksV0FBWCxFQUF1QjtBQUNuQmhDLEtBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZUssSUFBZixDQUFvQkwsQ0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFlSyxJQUFmLEtBQXNCLGdCQUExQztBQUNILEdBRkQsTUFFTyxJQUFHMkIsSUFBSSxJQUFJLFlBQVgsRUFBd0I7QUFDM0JoQyxLQUFDLENBQUMsV0FBRCxDQUFELENBQWVLLElBQWYsQ0FBb0JMLENBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZUssSUFBZixLQUFzQixtQkFBMUM7QUFDSDtBQUdKLENBakRBLENBQUQsQzs7Ozs7Ozs7Ozs7QUNyUUEsdUMiLCJmaWxlIjoiYXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLypcbiAqIFdlbGNvbWUgdG8geW91ciBhcHAncyBtYWluIEphdmFTY3JpcHQgZmlsZSFcbiAqXG4gKiBXZSByZWNvbW1lbmQgaW5jbHVkaW5nIHRoZSBidWlsdCB2ZXJzaW9uIG9mIHRoaXMgSmF2YVNjcmlwdCBmaWxlXG4gKiAoYW5kIGl0cyBDU1MgZmlsZSkgaW4geW91ciBiYXNlIGxheW91dCAoYmFzZS5odG1sLnR3aWcpLlxuICovXG5cbi8vIGFueSBDU1MgeW91IGltcG9ydCB3aWxsIG91dHB1dCBpbnRvIGEgc2luZ2xlIGNzcyBmaWxlIChhcHAuY3NzIGluIHRoaXMgY2FzZSlcbmltcG9ydCAnLi9zdHlsZXMvYXBwLnNjc3MnO1xuXG4vLyBQcm9ibMOobWUgZGUgc2NvcGUgamUgY3JvaXNcbi8vIGltcG9ydCAkIGZyb20gJ2pxdWVyeSc7XG5pbXBvcnQgeyBNb2RhbCB9IGZyb20gJ2Jvb3RzdHJhcCdcbnJlcXVpcmUoJ2Jvb3RzdHJhcC1pY29ucy9mb250L2Jvb3RzdHJhcC1pY29ucy5jc3MnKTtcblxuXG4vLyBWYXJpYWJsZXMgZCfDqXRhdFxubGV0IGN1cnJlbnRQYWdlID0gJ2NhdGVnb3JpZXMnO1xubGV0IGN1cnJlbnRDYXRJZCA9ICcnO1xubGV0IGN1cnJlbnRUb3BpY0lkID0gJyc7XG5sZXQgcHJldmlvdXNUaXRsZSA9ICcnO1xuXG4vLyBPbmNsaWNrIHN1ciBsZXMgY2F0ZWdvcmllcyBwb3VyIGFmZmljaGVyIGxlcyB0b3BpY3MgY29ycmVzcG9uZGFudHNcbmZ1bmN0aW9uIHNldE9uY2xpY2tDYXRUaXRsZXMoKSB7XG4gICAgJCgnLmNhdC10aXRsZScpLm9uKCdjbGljaycsYXN5bmMgZnVuY3Rpb24oKSB7XG4gICAgICAgIGxldCBjYXRfaWQgPSB0aGlzLmdldEF0dHJpYnV0ZSgnY2F0LWlkJyk7XG4gICAgICAgIGxldCBjYXRfbmFtZSA9IHRoaXMuZ2V0QXR0cmlidXRlKCdjYXQtbmFtZScpO1xuICAgICAgICBwcmV2aW91c1RpdGxlID0gJCgnI2N1cnJlbnQtdGl0bGUnKS50ZXh0KCk7XG4gICAgICAgICQoJyNjdXJyZW50LXRpdGxlJykudGV4dChjYXRfbmFtZSk7XG4gICAgICAgICQoJyN0b3BpYy1wYWdlJykuc2hvdygpO1xuICAgICAgICAkKCcjY2F0LXBhZ2UnKS5oaWRlKCk7XG4gICAgICAgICQoJyNyZXR1cm4tYnRuJykuc2hvdygpO1xuICAgICAgICBzZXRUb3BpY3MoY2F0X2lkKTtcbiAgICAgICAgY3VycmVudFBhZ2UgPSAndG9waWNzJztcbiAgICAgICAgY3VycmVudENhdElkID0gY2F0X2lkO1xuICAgIH0pO1xufVxuXG4vLyBPbmNsaWNrIHN1ciBsZXMgdG9waWNzIHBvdXIgYWZmaWNoZXIgbGVzIHBvc3RzIGNvcnJlc3BvbmRhbnRzIGF2ZWMgdW4gZmV0Y2hcbmZ1bmN0aW9uIHNldE9uY2xpY2tUb3BpY1RpdGxlcygpIHtcbiAgICAkKCcudG9waWMtdGl0bGUnKS5vbignY2xpY2snLGFzeW5jIGZ1bmN0aW9uKCkge1xuICAgICAgICBsZXQgdG9waWNfaWQgPSB0aGlzLmdldEF0dHJpYnV0ZSgndG9waWMtaWQnKTtcbiAgICAgICAgbGV0IHRvcGljX3RpdGxlID0gdGhpcy5nZXRBdHRyaWJ1dGUoJ3RvcGljLXRpdGxlJyk7XG4gICAgICAgIHByZXZpb3VzVGl0bGUgPSAkKCcjY3VycmVudC10aXRsZScpLnRleHQoKTtcbiAgICAgICAgJCgnI2N1cnJlbnQtdGl0bGUnKS50ZXh0KHRvcGljX3RpdGxlKTtcbiAgICAgICAgJCgnI3Bvc3QtcGFnZScpLnNob3coKTtcbiAgICAgICAgJCgnI3RvcGljLXBhZ2UnKS5oaWRlKCk7XG4gICAgICAgIHNldFBvc3RzKHRvcGljX2lkKTtcbiAgICAgICAgY3VycmVudFBhZ2UgPSAncG9zdHMnO1xuICAgICAgICBjdXJyZW50VG9waWNJZCA9IHRvcGljX2lkO1xuICAgIH0pO1xufVxuXG4vLyBPbmNsaWNrIHN1ciBsZXMgY3JvaXggcG91ciBzdXBwcmltZXIgdW4gdG9waWNcbmZ1bmN0aW9uIHNldE9uY2xpY2tEZWxldGVUb3BpY3MoKSB7XG4gICAgJCgnLmRlbGV0ZS10b3BpYy1idG4nKS5vbignY2xpY2snLCBmdW5jdGlvbigpIHtcbiAgICAgICAgZmV0Y2goJy9kZWxldGUtdG9waWMvJyskKHRoaXMpLmF0dHIoJ2lkJykpXG4gICAgICAgIC50aGVuKHNldFRvcGljcyhjdXJyZW50Q2F0SWQpKTtcbiAgICB9KTtcbn1cblxuLy8gT25jbGljayBzdXIgbGVzIGNyb2l4IHBvdXIgc3VwcHJpbWVyIHVuIHBvc3RcbmZ1bmN0aW9uIHNldE9uY2xpY2tEZWxldGVQb3N0cygpIHtcbiAgICAkKCcuZGVsZXRlLXBvc3QtYnRuJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKSB7XG4gICAgICAgIGZldGNoKCcvZGVsZXRlLXBvc3QvJyskKHRoaXMpLmF0dHIoJ2lkJykpXG4gICAgICAgIC50aGVuKHNldFBvc3RzKGN1cnJlbnRUb3BpY0lkKSk7XG4gICAgfSk7XG59XG5cbi8vIEZldGNoIGV0IGFmZmljaGUgbGVzIHRvcGljcyBkZSBsYSBjYXTDqWdvcmllIGTDqXNpcsOpZVxuYXN5bmMgZnVuY3Rpb24gc2V0VG9waWNzKGNhdF9pZCkge1xuICAgIGxldCByZXNwb25zZSA9IGF3YWl0IGZldGNoKCcvZ2V0LXRvcGljcy8nK2NhdF9pZCk7XG4gICAgbGV0IHRvcGljX2xpc3QgPSBhd2FpdCByZXNwb25zZS5qc29uKCk7XG4gICAgJCgnI3RvcGljLWxpc3QnKS50ZXh0KCcnKTtcbiAgICBpZih0b3BpY19saXN0LnRvcGljcy5sZW5ndGggPT0gMCl7XG4gICAgICAgICQoJyN0b3BpYy1saXN0JykuaHRtbCgnPHAgc3R5bGU9XCJkaXNwbGF5OiBmbGV4O2p1c3RpZnktY29udGVudDogY2VudGVyO1wiPkF1Y3VuIHRvcGljIGRhbnMgY2UgZm9ydW0gOiBpbnNjcml2ZXogb3UgY29ubmVjdGV6LXZvdXMgZXQgY3LDqWV6LWVuIHVuICE8L3A+Jyk7XG4gICAgfVxuICAgIC8vIEFmZmljaGUgZHUgcGx1cyByw6ljZW50IGF1IHBsdXMgYW5jaWVuXG4gICAgdG9waWNfbGlzdC50b3BpY3MgPSB0b3BpY19saXN0LnRvcGljcy5yZXZlcnNlKCk7XG4gICAgdG9waWNfbGlzdC50b3BpY3MuZm9yRWFjaCh0b3BpYyA9PiB7XG4gICAgICAgIC8vIFRvcGljIGluZm9zXG4gICAgICAgIGxldCB0b3BpY19pbmZvcyA9ICQoJzxkaXYgaWQ9XCJ0b3BpYy1pbmZvc1wiPicpO1xuICAgICAgICB0b3BpY19pbmZvcy5hcHBlbmQoJzxwPicrdG9waWMuYXV0ZXVyKyc8L3A+Jyk7XG4gICAgICAgIC8vIGxldCBkYXRlID0gbmV3IERhdGUodG9waWMuZGF0ZSk7XG4gICAgICAgIC8vIHRvcGljX2luZm9zLmFwcGVuZCgnPHA+JytkYXRlLnRvTG9jYWxlU3RyaW5nKCkrJzwvcD4nKTtcbiAgICAgICAgbGV0IGRlbGV0ZV90b3BpYyA9ICQoJzxpIGNsYXNzPVwiYmkteC1zcXVhcmUtZmlsbCBkZWxldGUtdG9waWMtYnRuIG1sLTJcIiBzdHlsZT1cImNvbG9yOiByZWQ7XCI+PC9pPicpO1xuICAgICAgICBkZWxldGVfdG9waWMuYXR0cignaWQnLHRvcGljLmlkKTtcbiAgICAgICAgaWYocm9sZSAhPSAnUk9MRV9BRE1JTicpe1xuICAgICAgICAgICAgZGVsZXRlX3RvcGljLmhpZGUoKTtcbiAgICAgICAgfVxuICAgICAgICB0b3BpY19pbmZvcy5hcHBlbmQoZGVsZXRlX3RvcGljKTtcbiAgICAgICAgLy8gVG9waWMgY29udGFpbmVyXG4gICAgICAgIGxldCB0b3BpY19jb250YWluZXIgPSAkKCc8ZGl2IGNsYXNzPVwidG9waWMtY29udGFpbmVyXCI+PC9kaXY+Jyk7XG4gICAgICAgIHRvcGljX2NvbnRhaW5lci5hcHBlbmQoJzxhIGNsYXNzPVwidG9waWMtdGl0bGVcIiB0b3BpYy1pZD1cIicrdG9waWMuaWQrJ1wiIHRvcGljLXRpdGxlPVwiJyt0b3BpYy50aXRyZSsnXCI+Jyt0b3BpYy50aXRyZSsnPC9hPicpO1xuICAgICAgICB0b3BpY19jb250YWluZXIuYXBwZW5kKHRvcGljX2luZm9zKTtcbiAgICAgICAgJCgnI3RvcGljLWxpc3QnKS5hcHBlbmQodG9waWNfY29udGFpbmVyKTtcbiAgICB9KTtcbiAgICBzZXRPbmNsaWNrVG9waWNUaXRsZXMoKTtcbiAgICBzZXRPbmNsaWNrRGVsZXRlVG9waWNzKCk7XG59XG5cbi8vIEZldGNoIGV0IGFmZmljaGUgbGVzIHBvc3RzIGR1IHRvcGljIGTDqXNpcsOpXG5hc3luYyBmdW5jdGlvbiBzZXRQb3N0cyh0b3BpY19pZCkge1xuICAgIGxldCByZXNwb25zZSA9IGF3YWl0IGZldGNoKCcvZ2V0LXBvc3RzLycrdG9waWNfaWQpO1xuICAgIGxldCBwb3N0X2xpc3QgPSBhd2FpdCByZXNwb25zZS5qc29uKCk7XG4gICAgJCgnI3Bvc3QtbGlzdCcpLnRleHQoJycpO1xuICAgIHBvc3RfbGlzdC5wb3N0cy5mb3JFYWNoKHBvc3QgPT4ge1xuICAgICAgICAvLyBQb3N0IGhlYWRlclxuICAgICAgICBsZXQgcG9zdF9oZWFkZXIgPSAkKCc8ZGl2IGNsYXNzPVwicG9zdC1oZWFkZXJcIj48L2Rpdj4nKTtcbiAgICAgICAgbGV0IGF1dGhvciA9ICQoJzxwPicrcG9zdC5hdXRldXIrJzwvcD4nKTtcbiAgICAgICAgbGV0IGRhdGUgPSBuZXcgRGF0ZShwb3N0LmRhdGUpO1xuICAgICAgICBsZXQgZGF0ZV9zdHJpbmcgPSAkKCc8cD4nK2RhdGUudG9Mb2NhbGVTdHJpbmcoKSsnPC9wPicpO1xuICAgICAgICBsZXQgZGVsZXRlX3Bvc3QgPSAkKCc8aSBjbGFzcz1cImJpLXgtc3F1YXJlLWZpbGwgZGVsZXRlLXBvc3QtYnRuXCIgc3R5bGU9XCJjb2xvcjogcmVkO1wiPjwvaT4nKTtcbiAgICAgICAgZGVsZXRlX3Bvc3QuYXR0cignaWQnLHBvc3QuaWQpO1xuICAgICAgICBpZihyb2xlICE9ICdST0xFX0FETUlOJyl7XG4gICAgICAgICAgICBkZWxldGVfcG9zdC5oaWRlKCk7XG4gICAgICAgIH1cbiAgICAgICAgcG9zdF9oZWFkZXIuYXBwZW5kKGF1dGhvciwgZGF0ZV9zdHJpbmcsIGRlbGV0ZV9wb3N0KTtcbiAgICAgICAgLy8gUG9zdCBjb250YWluZXJcbiAgICAgICAgbGV0IHBvc3RfY29udGFpbmVyID0gJCgnPGRpdiBjbGFzcz1cInBvc3QtY29udGFpbmVyXCI+PC9kaXY+Jyk7XG4gICAgICAgIGxldCB0ZXh0ID0gJCgnPGRpdiBjbGFzcz1cInRleHQtY29udGFpbmVyXCI+PHAgaWQ9XCInK3Bvc3QuaWQrJ1wiPicrcG9zdC50ZXh0ZSsnPC9wPjwvZGl2PicpO1xuICAgICAgICBwb3N0X2NvbnRhaW5lci5hcHBlbmQocG9zdF9oZWFkZXIsIHRleHQpO1xuXG4gICAgICAgICQoJyNwb3N0LWxpc3QnKS5hcHBlbmQocG9zdF9jb250YWluZXIpO1xuICAgIH0pO1xuICAgIHNldE9uY2xpY2tEZWxldGVQb3N0cygpO1xufVxuXG4vLyBCb3V0b24gcmV0b3VyXG5mdW5jdGlvbiByZXR1cm5CdG4oKXtcbiAgICBpZihjdXJyZW50UGFnZSA9PSAndG9waWNzJyl7XG4gICAgICAgICQoJyNjdXJyZW50LXRpdGxlJykudGV4dChcIkJpZW52ZW51ZVwiKTtcbiAgICAgICAgJCgnI3RvcGljLXBhZ2UnKS5oaWRlKCk7XG4gICAgICAgICQoJyNjYXQtcGFnZScpLnNob3coKTtcbiAgICAgICAgJCgnI3JldHVybi1idG4nKS5oaWRlKCk7XG4gICAgICAgIGN1cnJlbnRQYWdlID0gJ2NhdGVnb3JpZXMnO1xuICAgIH1lbHNlIGlmKGN1cnJlbnRQYWdlID09ICdwb3N0cycpe1xuICAgICAgICAkKCcjY3VycmVudC10aXRsZScpLnRleHQocHJldmlvdXNUaXRsZSk7XG4gICAgICAgICQoJyNwb3N0LXBhZ2UnKS5oaWRlKCk7XG4gICAgICAgICQoJyN0b3BpYy1wYWdlJykuc2hvdygpO1xuICAgICAgICBzZXRUb3BpY3MoY3VycmVudENhdElkKTtcbiAgICAgICAgY3VycmVudFBhZ2UgPSAndG9waWNzJztcbiAgICB9XG59XG5cbi8vIEFmZmljaGUgbGUgcHNldWRvIGFwcsOocyB1bmUgY29ubmV4aW9uIGV0IGluaXRpYWxpc2UgbGVzIGZvbmN0aW9ubmFsaXTDqXMgYXNzb2Npw6llc1xuZnVuY3Rpb24gaW5pdF9jb25uZXhpb24odXNlcm5hbWUsIG5ld19yb2xlKSB7XG4gICAgLy8gUG91ciB0b3V0IGxlcyBjb25uZWN0w6lzXG4gICAgJCgnI3NpZ24taW4nKS5tb2RhbCgnaGlkZScpO1xuICAgICQoXCIjdXNlcm5hbWVcIikudGV4dCh1c2VybmFtZSk7XG4gICAgJChcIiNzZW5kLXRvcGljLWJsb2NrXCIpLmNzcyhcImRpc3BsYXlcIixcImZsZXhcIik7XG4gICAgJChcIiNzZW5kLXBvc3QtYmxvY2tcIikuY3NzKFwiZGlzcGxheVwiLFwiZmxleFwiKTtcbiAgICAkKFwiI2Nvbm5lY3QtYmxvY2tcIikuaGlkZSgpO1xuICAgICQoXCIjdXNlci1ibG9ja1wiKS5zaG93KCk7XG4gICAgJChcIiN1c2VyLWJsb2NrXCIpLmNzcygnZGlzcGxheScsJ2ZsZXgnKTtcbiAgICAvLyBSZXNlcnbDqSBhdSBhZG1pbnNcbiAgICBpZihuZXdfcm9sZSA9PSAnUk9MRV9BRE1JTicpe1xuICAgICAgICByb2xlID0gJ1JPTEVfQURNSU4nO1xuICAgICAgICAkKFwiI3VzZXJuYW1lXCIpLnRleHQoJChcIiN1c2VybmFtZVwiKS50ZXh0KCkrJyBbdXRpbGlzYXRldXJdJyk7XG4gICAgICAgICQoJy5kZWxldGUtdG9waWMtYnRuJykuc2hvdygpO1xuICAgICAgICAkKCcuZGVsZXRlLXBvc3QtYnRuJykuc2hvdygpO1xuICAgICAgICBzZXRPbmNsaWNrRGVsZXRlVG9waWNzKCk7XG4gICAgICAgIHNldE9uY2xpY2tEZWxldGVQb3N0cygpO1xuICAgIH1lbHNlIGlmKG5ld19yb2xlID09ICdST0xFX1VTRVInKXtcbiAgICAgICAgJChcIiN1c2VybmFtZVwiKS50ZXh0KCQoXCIjdXNlcm5hbWVcIikudGV4dCgpKycgW2FkbWluaXN0cmF0ZXVyXScpO1xuICAgIH1cbn1cblxuLy8gRW52b2llIHVuIGZldGNoIHBvdXIgc2UgY29ubmVjdGVyIGF2ZWMgbGUgbW9kYWwgZGUgY29ubmV4aW9uXG5mdW5jdGlvbiBjb25uZXhpb24oKSB7XG4gICAgY29uc3QgZGF0YSA9IHtcbiAgICAgICAgdXNlcm5hbWU6ICQoJyNzaWduLWluLXVzZXJuYW1lJykudmFsKCksXG4gICAgICAgIHBhc3N3b3JkOiAkKCcjc2lnbi1pbi1wYXNzd29yZCcpLnZhbCgpXG4gICAgfTtcbiAgICBmZXRjaCgnL2xvZ2luJywge1xuICAgICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgfSxcbiAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoZGF0YSksXG4gICAgfSlcbiAgICAudGhlbiggYXN5bmMgZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgaWYgKHJlc3BvbnNlLm9rKSB7XG4gICAgICAgICAgICBsZXQgdXNlcm5hbWUgPSBkYXRhLnVzZXJuYW1lO1xuICAgICAgICAgICAgbGV0IG5ld19yb2xlID0gYXdhaXQgcmVzcG9uc2UudGV4dCgpO1xuICAgICAgICAgICAgaW5pdF9jb25uZXhpb24odXNlcm5hbWUsIG5ld19yb2xlKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICQoJyNiYWQtY3JlZGVudGlhbHMnKS5jc3MoJ2Rpc3BsYXknLCdpbmxpbmUnKTtcbiAgICAgICAgfVxuICAgIH0pXG4gICAgLy8gQSBhbWVsaW9yZXJcbiAgICAuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpICk7XG59XG5cbi8vIEVudm9pZSB1biBmZXRjaCBwb3VyIHNlIGRlY29ubmVjdGVyIChhc3luYyBuw6ljw6lzc2FpcmUgcG91ciDDqXZpdGVyIG5zX2JpbmRpbmdfYWJvcnRlZCApXG5hc3luYyBmdW5jdGlvbiBkZWNvbm5leGlvbigpIHtcbiAgICBsZXQgbG9nb3V0ID0gYXdhaXQgZmV0Y2goJy9sb2dvdXQnKTtcbiAgICBsb2NhdGlvbi5yZWxvYWQoKTtcbn1cblxuLy8gRW52b2llIHVuIGZldGNoIHBvdXIgcydpbnNjcmlyZSBhdmVjIGxlIG1vZGFsIGQnaW5zY3JpcHRpb25cbmZ1bmN0aW9uIGluc2NyaXB0aW9uKCkge1xuICAgIGNvbnN0IGRhdGEgPSB7XG4gICAgICAgIHVzZXJuYW1lOiAkKCcjc2lnbi11cC11c2VybmFtZScpLnZhbCgpLFxuICAgICAgICBwYXNzd29yZDogJCgnI3NpZ24tdXAtcGFzc3dvcmQnKS52YWwoKVxuICAgIH07XG4gICAgZmV0Y2goJy9yZWdpc3RlcicsIHtcbiAgICAgICAgbWV0aG9kOiAnUE9TVCcsXG4gICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIH0sXG4gICAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KGRhdGEpLFxuICAgIH0pXG4gICAgLnRoZW4oIGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgIGlmIChyZXNwb25zZS5vaykge1xuICAgICAgICAgICAgJChcIiNzaWduLXVwXCIpLm1vZGFsKFwiaGlkZVwiKVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgJCgnI2JhZC11c2VybmFtZScpLmNzcygnZGlzcGxheScsJ2lubGluZScpO1xuICAgICAgICB9XG4gICAgfSlcbiAgICAvLyBBIGFtZWxpb3JlclxuICAgIC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikgKTtcbn1cblxuLy8gRW52b2llIHVuIGZldGNoIHBvdXIgcG9zdGVyIHVuIHRvcGljXG5mdW5jdGlvbiBzZW5kX3RvcGljKCkge1xuICAgIGNvbnN0IGRhdGEgPSB7XG4gICAgICAgIHRpdGxlOiAkKCcjc2VuZC10b3BpYy10aXRsZScpLnZhbCgpLFxuICAgICAgICBtZXNzYWdlOiAkKCcjc2VuZC10b3BpYy10ZXh0JykudmFsKCksXG4gICAgICAgIGNhdF9pZDogY3VycmVudENhdElkXG4gICAgfTtcbiAgICBmZXRjaCgnL3NlbmQtdG9waWMnLCB7XG4gICAgICAgIG1ldGhvZDogJ1BPU1QnLFxuICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICB9LFxuICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeShkYXRhKSxcbiAgICB9KVxuICAgIC50aGVuKHNldFRvcGljcyhjdXJyZW50Q2F0SWQpKVxuICAgIC8vIEEgYW1lbGlvcmVyXG4gICAgLmNhdGNoKGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKSApO1xufVxuXG4vLyBFbnZvaWUgdW4gZmV0Y2ggcG91ciBlbnZveWVyIHVuIHBvc3RcbmZ1bmN0aW9uIHNlbmRfcG9zdCgpIHtcbiAgICBjb25zdCBkYXRhID0ge1xuICAgICAgICBtZXNzYWdlOiAkKCcjc2VuZC1wb3N0LXRleHQnKS52YWwoKSxcbiAgICAgICAgdG9waWNfaWQ6IGN1cnJlbnRUb3BpY0lkXG4gICAgfTtcbiAgICBmZXRjaCgnL3NlbmQtYS1wb3N0Jywge1xuICAgICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgfSxcbiAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoZGF0YSksXG4gICAgfSlcbiAgICAudGhlbihzZXRQb3N0cyhjdXJyZW50VG9waWNJZCkpXG4gICAgLy8gQSBhbWVsaW9yZXJcbiAgICAuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpICk7XG59XG5cbiQoZnVuY3Rpb24oKXtcblxuICAgIC8vIE9uIGluaXRpYWxpc2UgbGVzIGNhdGVnb3JpZXMgZXQgb24gY2FjaGUgbGVzIGF1dHJlcyB2b2xldHNcbiAgICBzZXRPbmNsaWNrQ2F0VGl0bGVzKCk7XG4gICAgJCgnI3RvcGljLXBhZ2UnKS5oaWRlKCk7XG4gICAgJCgnI3Bvc3QtcGFnZScpLmhpZGUoKTtcblxuICAgIC8vIEJvdXRvbiByZXRvdXJcbiAgICAkKCcjcmV0dXJuLWJ0bicpLmhpZGUoKTtcbiAgICAkKCcjcmV0dXJuLWJ0bicpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCl7XG4gICAgICAgIHJldHVybkJ0bigpO1xuICAgIH0pO1xuXG4gICAgLy8gQ29ubmV4aW9uXG4gICAgJCgnI3NpZ24taW4tc3VibWl0Jykub24oJ2NsaWNrJywgZnVuY3Rpb24oKXtcbiAgICAgICAgY29ubmV4aW9uKCk7XG4gICAgfSk7XG5cbiAgICAvLyBEZWNvbm5leGlvblxuICAgICQoJyNsb2dvdXQtYnRuJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKXtcbiAgICAgICAgZGVjb25uZXhpb24oKTtcbiAgICB9KTtcblxuICAgIC8vIEluc2NyaXB0aW9uXG4gICAgJCgnI3NpZ24tdXAtc3VibWl0Jykub24oJ2NsaWNrJywgZnVuY3Rpb24oKXtcbiAgICAgICAgaWYoICQoJyNzaWduLXVwLXBhc3N3b3JkJykudmFsKCkgPT0gJCgnI3NpZ24tdXAtcGFzc3dvcmQtMicpLnZhbCgpICl7XG4gICAgICAgICAgICBpbnNjcmlwdGlvbigpO1xuICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgICQoJyNiYWQtcGFzc3dvcmRzJykuY3NzKCdkaXNwbGF5JywnaW5saW5lJyk7XG4gICAgICAgIH1cbiAgICB9KTtcblxuICAgIC8vIEVudm9pIHRvcGljXG4gICAgJCgnI3N1Ym1pdC10b3BpYycpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCl7XG4gICAgICAgIHNlbmRfdG9waWMoKTtcbiAgICB9KTtcblxuICAgIC8vIEVudm9pIHBvc3RcbiAgICAkKCcjc3VibWl0LXBvc3QnKS5vbignY2xpY2snLCBmdW5jdGlvbigpe1xuICAgICAgICBzZW5kX3Bvc3QoKTtcbiAgICB9KTtcblxuICAgIGlmKHJvbGUgPT0gJ1JPTEVfVVNFUicpe1xuICAgICAgICAkKCcjdXNlcm5hbWUnKS50ZXh0KCQoJyN1c2VybmFtZScpLnRleHQoKSsnIFt1dGlsaXNhdGV1cl0nKTtcbiAgICB9IGVsc2UgaWYocm9sZSA9PSAnUk9MRV9BRE1JTicpe1xuICAgICAgICAkKCcjdXNlcm5hbWUnKS50ZXh0KCQoJyN1c2VybmFtZScpLnRleHQoKSsnIFthZG1pbmlzdHJhdGV1cl0nKTtcbiAgICB9XG4gICAgXG5cbn0pXG4iLCIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW4iXSwic291cmNlUm9vdCI6IiJ9