/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

// Problème de scope je crois
// import $ from 'jquery';
import { Modal } from 'bootstrap'
require('bootstrap-icons/font/bootstrap-icons.css');


// Variables d'état
let currentPage = 'categories';
let currentCatId = '';
let currentTopicId = '';
let previousTitle = '';

// Onclick sur les categories pour afficher les topics correspondants
function setOnclickCatTitles() {
    $('.cat-title').on('click',async function() {
        let cat_id = this.getAttribute('cat-id');
        let cat_name = this.getAttribute('cat-name');
        previousTitle = $('#current-title').text();
        $('#current-title').text(cat_name);
        $('#topic-page').show();
        $('#cat-page').hide();
        $('#return-btn').show();
        setTopics(cat_id);
        currentPage = 'topics';
        currentCatId = cat_id;
    });
}

// Onclick sur les topics pour afficher les posts correspondants avec un fetch
function setOnclickTopicTitles() {
    $('.topic-title').on('click',async function() {
        let topic_id = this.getAttribute('topic-id');
        let topic_title = this.getAttribute('topic-title');
        previousTitle = $('#current-title').text();
        $('#current-title').text(topic_title);
        $('#post-page').show();
        $('#topic-page').hide();
        setPosts(topic_id);
        currentPage = 'posts';
        currentTopicId = topic_id;
    });
}

// Onclick sur les croix pour supprimer un topic
function setOnclickDeleteTopics() {
    $('.delete-topic-btn').on('click', function() {
        fetch('/delete-topic/'+$(this).attr('id'))
        .then(setTopics(currentCatId));
    });
}

// Onclick sur les croix pour supprimer un post
function setOnclickDeletePosts() {
    $('.delete-post-btn').on('click', function() {
        fetch('/delete-post/'+$(this).attr('id'))
        .then(setPosts(currentTopicId));
    });
}

// Fetch et affiche les topics de la catégorie désirée
async function setTopics(cat_id) {
    let response = await fetch('/get-topics/'+cat_id);
    let topic_list = await response.json();
    $('#topic-list').text('');
    if(topic_list.topics.length == 0){
        $('#topic-list').html('<p style="display: flex;justify-content: center;">Aucun topic dans ce forum : inscrivez ou connectez-vous et créez-en un !</p>');
    }
    // Affiche du plus récent au plus ancien
    topic_list.topics = topic_list.topics.reverse();
    topic_list.topics.forEach(topic => {
        // Topic infos
        let topic_infos = $('<div id="topic-infos">');
        topic_infos.append('<p>'+topic.auteur+'</p>');
        // let date = new Date(topic.date);
        // topic_infos.append('<p>'+date.toLocaleString()+'</p>');
        let delete_topic = $('<i class="bi-x-square-fill delete-topic-btn ml-2" style="color: red;"></i>');
        delete_topic.attr('id',topic.id);
        if(role != 'ROLE_ADMIN'){
            delete_topic.hide();
        }
        topic_infos.append(delete_topic);
        // Topic container
        let topic_container = $('<div class="topic-container"></div>');
        topic_container.append('<a class="topic-title" topic-id="'+topic.id+'" topic-title="'+topic.titre+'">'+topic.titre+'</a>');
        topic_container.append(topic_infos);
        $('#topic-list').append(topic_container);
    });
    setOnclickTopicTitles();
    setOnclickDeleteTopics();
}

// Fetch et affiche les posts du topic désiré
async function setPosts(topic_id) {
    let response = await fetch('/get-posts/'+topic_id);
    let post_list = await response.json();
    $('#post-list').text('');
    post_list.posts.forEach(post => {
        // Post header
        let post_header = $('<div class="post-header"></div>');
        let author = $('<p>'+post.auteur+'</p>');
        let date = new Date(post.date);
        let date_string = $('<p>'+date.toLocaleString()+'</p>');
        let delete_post = $('<i class="bi-x-square-fill delete-post-btn" style="color: red;"></i>');
        delete_post.attr('id',post.id);
        if(role != 'ROLE_ADMIN'){
            delete_post.hide();
        }
        post_header.append(author, date_string, delete_post);
        // Post container
        let post_container = $('<div class="post-container"></div>');
        let text = $('<div class="text-container"><p id="'+post.id+'">'+post.texte+'</p></div>');
        post_container.append(post_header, text);

        $('#post-list').append(post_container);
    });
    setOnclickDeletePosts();
}

// Bouton retour
function returnBtn(){
    if(currentPage == 'topics'){
        $('#current-title').text("Bienvenue");
        $('#topic-page').hide();
        $('#cat-page').show();
        $('#return-btn').hide();
        currentPage = 'categories';
    }else if(currentPage == 'posts'){
        $('#current-title').text(previousTitle);
        $('#post-page').hide();
        $('#topic-page').show();
        setTopics(currentCatId);
        currentPage = 'topics';
    }
}

// Affiche le pseudo après une connexion et initialise les fonctionnalités associées
function init_connexion(username, new_role) {
    // Pour tout les connectés
    $('#sign-in').modal('hide');
    $("#username").text(username);
    $("#send-topic-block").css("display","flex");
    $("#send-post-block").css("display","flex");
    $("#connect-block").hide();
    $("#user-block").show();
    $("#user-block").css('display','flex');
    // Reservé au admins
    if(new_role == 'ROLE_ADMIN'){
        role = 'ROLE_ADMIN';
        $("#username").text($("#username").text()+' [utilisateur]');
        $('.delete-topic-btn').show();
        $('.delete-post-btn').show();
        setOnclickDeleteTopics();
        setOnclickDeletePosts();
    }else if(new_role == 'ROLE_USER'){
        $("#username").text($("#username").text()+' [administrateur]');
    }
}

// Envoie un fetch pour se connecter avec le modal de connexion
function connexion() {
    const data = {
        username: $('#sign-in-username').val(),
        password: $('#sign-in-password').val()
    };
    fetch('/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    })
    .then( async function(response) {
        if (response.ok) {
            let username = data.username;
            let new_role = await response.text();
            init_connexion(username, new_role);
        } else {
            $('#bad-credentials').css('display','inline');
        }
    })
    // A ameliorer
    .catch(error => console.log(error) );
}

// Envoie un fetch pour se deconnecter (async nécéssaire pour éviter ns_binding_aborted )
async function deconnexion() {
    let logout = await fetch('/logout');
    location.reload();
}

// Envoie un fetch pour s'inscrire avec le modal d'inscription
function inscription() {
    const data = {
        username: $('#sign-up-username').val(),
        password: $('#sign-up-password').val()
    };
    fetch('/register', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    })
    .then( function(response) {
        if (response.ok) {
            $("#sign-up").modal("hide")
        } else {
            $('#bad-username').css('display','inline');
        }
    })
    // A ameliorer
    .catch(error => console.log(error) );
}

// Envoie un fetch pour poster un topic
function send_topic() {
    const data = {
        title: $('#send-topic-title').val(),
        message: $('#send-topic-text').val(),
        cat_id: currentCatId
    };
    fetch('/send-topic', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    })
    .then(setTopics(currentCatId))
    // A ameliorer
    .catch(error => console.log(error) );
}

// Envoie un fetch pour envoyer un post
function send_post() {
    const data = {
        message: $('#send-post-text').val(),
        topic_id: currentTopicId
    };
    fetch('/send-a-post', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    })
    .then(setPosts(currentTopicId))
    // A ameliorer
    .catch(error => console.log(error) );
}

$(function(){

    // On initialise les categories et on cache les autres volets
    setOnclickCatTitles();
    $('#topic-page').hide();
    $('#post-page').hide();

    // Bouton retour
    $('#return-btn').hide();
    $('#return-btn').on('click', function(){
        returnBtn();
    });

    // Connexion
    $('#sign-in-submit').on('click', function(){
        connexion();
    });

    // Deconnexion
    $('#logout-btn').on('click', function(){
        deconnexion();
    });

    // Inscription
    $('#sign-up-submit').on('click', function(){
        if( $('#sign-up-password').val() == $('#sign-up-password-2').val() ){
            inscription();
        }else{
            $('#bad-passwords').css('display','inline');
        }
    });

    // Envoi topic
    $('#submit-topic').on('click', function(){
        send_topic();
    });

    // Envoi post
    $('#submit-post').on('click', function(){
        send_post();
    });

    if(role == 'ROLE_USER'){
        $('#username').text($('#username').text()+' [utilisateur]');
    } else if(role == 'ROLE_ADMIN'){
        $('#username').text($('#username').text()+' [administrateur]');
    }
    

})
