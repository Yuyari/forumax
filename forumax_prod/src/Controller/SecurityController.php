<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Security;

use App\Entity\User;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="login", methods={"GET", "POST"})
     */
    public function login(AuthenticationUtils $authenticationUtils, Security $security){
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $user_role = implode($security->getUser()->getRoles());

        $response = new Response(
            $user_role,
            Response::HTTP_OK,
            ['content-type' => 'text/html']
        );

        return $response;
    }

    /**
     * @Route("/register", name="register", methods={"GET", "POST"})
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder){
        $data = json_decode($request->getContent(), true);
        $username = $data['username'];
        $password = $data['password'];
        
        $entityManager = $this->getDoctrine()->getManager();
        $user = new User();
        $user->setUsername($username);
        $user->setPassword($encoder->encodePassword($user,$password));
        $user->setRoles(['ROLE_USER']);
        $entityManager->persist($user);
        $entityManager->flush();

        $response = new Response(
            'Content',
            Response::HTTP_OK
        );

        return $response;
    }

    /**
     * @Route("/logout", name="logout", methods={"GET"})
     */
    public function logout(){}
}


?>