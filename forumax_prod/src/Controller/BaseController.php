<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;

use App\Entity\Category;
use App\Entity\Topic;
use App\Entity\Post;

class BaseController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function accueil(LoggerInterface $logger)
    {
        $categories = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findAll();

        if( $this->isGranted('IS_AUTHENTICATED_REMEMBERED') ){
            $username = $this->getUser()->getUsername();
        }else{
            $username = "";
        }

        return $this->render('base.html.twig', ['categories' => $categories, 'username' => $username]);
    }

    /**
     * @Route("/get-categories", name="get-categories")
     */
    public function getCategories(SerializerInterface $serializer)
    {
        $categories = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findAll();

        $liste_categories = array('categories' => $categories);
        $liste_categories = $serializer->serialize($liste_categories, 'json');

        $response = new Response(
            $liste_categories,
            Response::HTTP_OK,
            ['content-type' => 'application/json']
        );

        return $response;
    }

    /**
     * @Route("/get-topics/{cat_id}", name="get-topics-by-cat-id")
     */
    public function getTopicsByCategoryName(SerializerInterface $serializer, $cat_id)
    {
        $sujets = $this->getDoctrine()
            ->getRepository(Topic::class)
            ->findBy(array('categorie' => $cat_id));

        $categorie = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findBy(array('id' => $cat_id));

        $liste_topics = array(
            'category-details' => $categorie,
            'topics' => $sujets
        );
        
        $liste_topics = $serializer->serialize($liste_topics, 'json');

        $response = new Response(
            $liste_topics,
            Response::HTTP_OK,
            ['content-type' => 'application/json']
        );

        return $response;
    }

    /**
     * @Route("/get-posts/{topic_id}", name="get-posts-by-topic-id")
     */
    public function getPosts(SerializerInterface $serializer, $topic_id)
    {
        $topic = $this->getDoctrine()
            ->getRepository(Topic::class)
            ->findBy(array('id' => $topic_id));

        $posts = $this->getDoctrine()
            ->getRepository(Post::class)
            ->findBy(array('sujet' => $topic_id));

        $liste_posts = array(
            'topic-details' => $topic,
            'posts' => $posts
        );
        $liste_posts = $serializer->serialize($liste_posts, 'json');

        $response = new Response(
            $liste_posts,
            Response::HTTP_OK,
            ['content-type' => 'application/json']
        );

        return $response;
    }

    /**
     * @Route("/send-topic", name="send-topic", methods={"POST"})
     */
    public function sendTopic(Request $request, LoggerInterface $logger)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        $data = json_decode($request->getContent(), true);
        $titre = $data['title'];
        $message = $data['message'];
        $categorie = $data['cat_id'];
        $auteur = $this->getUser()->getUsername();
        $dateTime = new \DateTime(strftime('%Y-%m-%d %H:%M:%S'));
        $id = uniqid();

        $entityManager = $this->getDoctrine()->getManager();

        // On crée le topic
        $topic = new Topic(); 
        $topic->setId($id);
        $topic->setTitre($titre);
        $topic->setCategorie($categorie);
        $topic->setAuteur($auteur);
        $topic->setDate($dateTime);
        $entityManager->persist($topic);

        // Et le message, considéré comme post N°1
        $post = new Post();
        $post->setSujet($id);
        $post->setAuteur($auteur);
        $post->setDate($dateTime);
        $post->setTexte($message);
        $entityManager->persist($post);

        $entityManager->flush();

        $response = new Response(
            'Topic envoyé avec succès',
            Response::HTTP_OK
        );

        return $response;
    }

    /**
     * @Route("/send-post", name="send-post", methods={"POST"})
     */
    public function sendPost(Request $request, LoggerInterface $logger)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        $data = json_decode($request->getContent(), true);
        $message = $data['message'];
        $sujet = $data['topic_id'];
        $auteur = $this->getUser()->getUsername();
        $dateTime = new \DateTime(strftime('%Y-%m-%d %H:%M:%S'));

        $entityManager = $this->getDoctrine()->getManager();

        $post = new Post();
        $post->setSujet($sujet);
        $post->setAuteur($auteur);
        $post->setDate($dateTime);
        $post->setTexte($message);

        $entityManager->persist($post);
        $entityManager->flush();

        $response = new Response(
            'Post envoyé avec succès',
            Response::HTTP_OK
        );

        return $response;
    }

        /**
     * @Route("/delete-topic/{topic_id}", name="delete-topic", methods={"GET"})
     */
    public function deleteTopic(Request $request, LoggerInterface $logger, $topic_id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $conn = $this->getDoctrine()->getManager()->getConnection();

        $sql = 'DELETE FROM topic WHERE "id" = :topic_id';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['topic_id' => $topic_id]);

        $response = new Response(
            'Topic supprimé avec succès',
            Response::HTTP_OK
        );

        return $response;
    }

    /**
     * @Route("/delete-post/{post_id}", name="delete-post", methods={"GET"})
     */
    public function deletePost(Request $request, LoggerInterface $logger, $post_id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $conn = $this->getDoctrine()->getManager()->getConnection();

        $sql = 'DELETE FROM post WHERE "id" = :post_id';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['post_id' => $post_id]);

        $response = new Response(
            'Topic supprimé avec succès',
            Response::HTTP_OK
        );

        return $response;
    }
}

?>