PROJET PHP/JS

Auteur : Yuyari SALAS, LP2 (projet réalisé seul)

Le projet que j'ai choisis est donc le forum, je l'ai fait en Single Page Application.

Il n'y a qu'une seule page (forcément), mais on peut distinguer trois états d'affichage différents:
- Les categories, ou on choisit le theme des discussions
- En cliquant sur une catégorie on nous affiche la liste des sujets sur la categorie choisie
- En cliquant sur un sujet on nous affichage les messages envoyés sur ce sujet

Le forum dispose d'un bouton de retour dynamique.

A tout moment il est possible de s'inscire ou se connecter au forum.
Si on est un simple utilisateur, des formulaires d'envoi pour poster un sujet ou un message apparaitrons après la connection.
De plus, si on est un administrateur, des petites icones rouges apparaitrons sur les sujets ou les posts pour permettre de les supprimer.
On peut donc modérer le forum, mais uniquement si on est administrateur.

Le forum dispose d'une API pour récupérer:
- Les categories (/get-categories)
- Les sujets par id de catégorie (/get-topics/id)
- Les posts par id de sujet (get-posts/id)

Le forum porte aussi un theme responsive.